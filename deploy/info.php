<?php

/////////////////////////////////////////////////////////////////////////////
// General information
/////////////////////////////////////////////////////////////////////////////

$app['basename'] = 'shell_extension';
$app['version'] = '2.5.0';
$app['vendor'] = 'ClearFoundation';
$app['packager'] = 'ClearFoundation';
$app['license'] = 'GPLv3';
$app['license_core'] = 'LGPLv3';
$app['description'] = lang('shell_extension_app_description');

/////////////////////////////////////////////////////////////////////////////
// App name and categories
/////////////////////////////////////////////////////////////////////////////

$app['name'] = lang('shell_extension_app_name');
$app['category'] = lang('base_category_system');
$app['subcategory'] = lang('base_subcategory_account_manager');
$app['menu_enabled'] = FALSE;

/////////////////////////////////////////////////////////////////////////////
// Packaging
/////////////////////////////////////////////////////////////////////////////

$app['core_only'] = TRUE;

$app['core_requires'] = array(
    'app-openldap-directory-core',
    'app-users-core',
);

$app['core_file_manifest'] = array( 
   'shell.php' => array(
        'target' => '/var/clearos/openldap_directory/extensions/10_shell.php'
    ),
);

$app['delete_dependency'] = array();
